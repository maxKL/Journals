package com.maximov.blo.services;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.filters.ListFilter;
import com.maximov.common.filters.SingleFilter;
import com.maximov.common.models.enums.Role;
import com.maximov.common.models.pojo.Person;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Сервис для работы с пользователями
 */
public interface PersonService extends CrudService<Person> {
    @Override
    @Secured("ROLE_ADMIN")
    List<Person> getList() throws ServiceException;

    @Override
    @Secured("ROLE_ADMIN")
    List<Person> getList(ListFilter listFilter) throws ServiceException;

    @Override
    @Secured("ROLE_ADMIN")
    Person getById(int id) throws ServiceException;

    @Override
    @Secured("ROLE_ADMIN")
    Person get(SingleFilter filter) throws ServiceException;

    @Override
    @Secured("NONE")
    void create(Person entity) throws ServiceException;

    @Override
    @Secured("NONE")
    void update(Person entity) throws ServiceException;

    @Override
    @Secured("NONE")
    void delete(int id) throws ServiceException;

    /**
     * Блокировать пользователя
     * @param id Идентификатор пользователя
     */
    @Secured("ROLE_ADMIN")
    void blockPerson(int id) throws ServiceException;

    /**
     * Разблокировать пользователя
     * @param id Идентификатор пользователя
     */
    @Secured("ROLE_ADMIN")
    void unblockPerson(int id) throws ServiceException;

    /**
     * Установить пользователю роль
     * @param id Идентификатор пользователя
     * @param role Роль
     */
    @Secured("ROLE_ADMIN")
    void setRole(int id, Role role) throws ServiceException;
}
