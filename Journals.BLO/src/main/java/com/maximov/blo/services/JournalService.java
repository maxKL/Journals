package com.maximov.blo.services;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.filters.ListFilter;
import com.maximov.common.filters.SingleFilter;
import com.maximov.common.models.pojo.Journal;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Сервис для работы с дневниками
 */
@Secured("ROLE_USER")
public interface JournalService extends CrudService<Journal> {
    @Override
    List<Journal> getList() throws ServiceException;

    @Override
    List<Journal> getList(ListFilter listFilter) throws ServiceException;

    @Override
    Journal getById(int id) throws ServiceException;

    @Override
    Journal get(SingleFilter filter) throws ServiceException;

    @Override
    void create(Journal entity) throws ServiceException;

    @Override
    void update(Journal entity) throws ServiceException;

    @Override
    void delete(int id) throws ServiceException;
}
