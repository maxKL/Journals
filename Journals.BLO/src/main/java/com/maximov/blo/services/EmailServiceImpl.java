package com.maximov.blo.services;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.*;
import java.util.Properties;

@org.springframework.stereotype.Service
public class EmailServiceImpl implements EmailService {
    private Logger logger = Logger.getLogger(EmailServiceImpl.class);

    private Session session;
    private String username;
    private String password;

    public EmailServiceImpl(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        try(InputStream stream = new FileInputStream("mailConfig.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, "utf-8"))) {
            username = bufferedReader.readLine();
            password = bufferedReader.readLine();
            session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public void sendMessage(String address, String subject, String messageText) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(address));
            message.setSubject(subject);
            message.setText(messageText);

            Transport.send(message);
        } catch (MessagingException e) {
            logger.error(e);
        }
    }
}
