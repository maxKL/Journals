package com.maximov.blo.services;

/**
 * Сервис для посылки сообщений по электронной почте
 */
public interface EmailService extends Service {
    void sendMessage(String address, String subject, String messageText);
}
