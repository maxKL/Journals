package com.maximov.blo.services;

import com.maximov.common.exceptions.NoteException;
import com.maximov.common.exceptions.ServiceException;
import com.maximov.dao.transformers.Transformer;
import com.maximov.common.models.pojo.Note;
import com.maximov.dao.entities.NoteEntity;
import com.maximov.dao.repositories.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Реализация сервиса для работы с заметками
 */
@org.springframework.stereotype.Service
public class NoteServiceImpl extends CrudServiceImpl<Note, NoteEntity> implements NoteService {
    @Autowired
    protected NoteServiceImpl(NoteRepository repository, Transformer<Note, NoteEntity> transformer) {
        super(repository, transformer);
    }


    @Override
    protected ServiceException createServiceException(String message) {
        return new NoteException(message);
    }
}
