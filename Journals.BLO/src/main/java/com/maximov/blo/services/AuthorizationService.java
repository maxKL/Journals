package com.maximov.blo.services;

import com.maximov.common.models.RegistrationInfo;
import com.maximov.common.exceptions.ServiceException;

public interface AuthorizationService extends Service {
    void register(RegistrationInfo registrationInfo) throws ServiceException;
}
