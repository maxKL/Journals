package com.maximov.blo.services;

import com.maximov.common.exceptions.PersonServiceException;
import com.maximov.common.exceptions.ServiceException;
import com.maximov.dao.transformers.Transformer;
import com.maximov.common.models.enums.Role;
import com.maximov.common.models.pojo.Person;
import com.maximov.dao.entities.PersonEntity;
import com.maximov.dao.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Сервис для работы с пользователями
 */
@org.springframework.stereotype.Service
public class PersonServiceImpl extends CrudServiceImpl<Person, PersonEntity> implements PersonService {

    @Autowired
    public PersonServiceImpl(PersonRepository repository, Transformer<Person, PersonEntity> transformer) {
        super(repository, transformer);
    }

    @Override
    protected ServiceException createServiceException(String message) {
        return new PersonServiceException(message);
    }

    @Override
    public void blockPerson(int id) throws ServiceException {
        try {
            PersonEntity personEntity = this.repository.findOne(id);
            personEntity.setBlock(true);
            this.repository.save(personEntity);
        } catch (Exception e) {
            throw onException("Не удалось заблокировать пользователя", e);
        }
    }

    @Override
    public void unblockPerson(int id) throws ServiceException {
        try {
            PersonEntity personEntity = this.repository.findOne(id);
            personEntity.setBlock(false);
            this.repository.save(personEntity);
        } catch (Exception e) {
            throw onException("Не удалось разблокировать пользователя", e);
        }
    }

    @Override
    public void setRole(int id, Role role) throws ServiceException {
        try {
            PersonEntity personEntity = this.repository.findOne(id);
            personEntity.setRole(role.getValue());
            this.repository.save(personEntity);
        } catch (Exception e) {
            throw onException("Не удалось установить роль пользователю", e);
        }
    }
}
