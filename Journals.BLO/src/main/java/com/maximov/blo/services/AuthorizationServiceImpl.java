package com.maximov.blo.services;

import com.maximov.common.exceptions.AuthorizeServiceException;
import com.maximov.common.models.enums.Role;
import com.maximov.common.models.RegistrationInfo;
import com.maximov.common.exceptions.ServiceException;
import com.maximov.dao.entities.PersonEntity;
import com.maximov.dao.repositories.CommonRepository;
import com.maximov.dao.repositories.PersonRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    private CommonRepository<PersonEntity> repository;
    private PasswordEncoder passwordEncoder;
    private Logger logger = Logger.getLogger(AuthorizationServiceImpl.class);

    @Autowired
    public void setRepository(PersonRepository repository) {
        this.repository = repository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public void register(RegistrationInfo registrationInfo) throws ServiceException {
        try {
            PersonEntity person = new PersonEntity();
            person.setLogin(registrationInfo.getLogin());
            String encodedPassword = this.passwordEncoder.encode(registrationInfo.getPassword());
            person.setPassword(encodedPassword);
            person.setRole(Role.ROLE_USER.getValue());
            person.setEmail(registrationInfo.getEmail());
            this.repository.save(person);
        } catch (Exception e) {
            logger.error(e);
            throw new AuthorizeServiceException("Ошибка регистрации пользователя");
        }
    }
}
