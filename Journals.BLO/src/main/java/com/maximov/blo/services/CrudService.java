package com.maximov.blo.services;

import com.maximov.common.filters.ListFilter;
import com.maximov.common.models.pojo.SimplePojo;
import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.filters.SingleFilter;

import java.util.List;

public interface CrudService<T extends SimplePojo> extends Service {
    List<T> getList() throws ServiceException;

    List<T> getList(ListFilter listFilter) throws ServiceException;

    T getById(int id) throws ServiceException;

    T get(SingleFilter filter) throws ServiceException;

    void create(T entity) throws ServiceException;

    void update(T entity) throws ServiceException;

    void delete(int id) throws ServiceException;
}
