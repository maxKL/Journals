package com.maximov.blo.services;

import com.maximov.common.filters.PersonSingleFilter;
import com.maximov.common.models.UserPrincipal;
import com.maximov.common.models.enums.Role;
import com.maximov.dao.entities.PersonEntity;
import com.maximov.dao.repositories.CommonRepository;
import com.maximov.dao.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class UserDetailServiceImpl implements LocalUserDetailsService {
    private CommonRepository<PersonEntity> repository;

    @Autowired
    public void setRepository(PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        PersonEntity user = repository.getByFilter(new PersonSingleFilter(username));

        if(user == null || user.isBlock())
            throw new UsernameNotFoundException("User '" + username + "' not found or blocked.");

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(Role.fromShort(user.getRole()).toString()));

        return new UserPrincipal(user.getId(),
                user.getLogin(),
                user.getPassword(),
                authorities);
    }
}
