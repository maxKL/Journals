package com.maximov.blo.services;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface LocalUserDetailsService extends UserDetailsService, Service {
}
