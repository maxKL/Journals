package com.maximov.blo.services;

import com.maximov.dao.transformers.Transformer;
import com.maximov.common.filters.ListFilter;
import com.maximov.common.models.pojo.SimplePojo;
import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.filters.SingleFilter;
import com.maximov.dao.entities.Entity;
import com.maximov.dao.repositories.CommonRepository;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public abstract class CrudServiceImpl<T extends SimplePojo, TEntity extends Entity> implements CrudService<T> {
    protected CommonRepository<TEntity> repository;
    protected Transformer<T, TEntity> transformer;
    private Logger logger = Logger.getLogger(this.getClass());

    protected CrudServiceImpl(CommonRepository<TEntity> repository, Transformer<T, TEntity> transformer) {
        this.repository = repository;
        this.transformer = transformer;
    }

    public T getById(int id) throws ServiceException {
        try {
            return transformer.convertEntityToPojo(this.repository.findOne(id));
        } catch (Exception e) {
            throw onException("Не удалось получить сущность", e);
        }
    }

    public T get(SingleFilter filter) throws ServiceException {
        try {
            return transformer.convertEntityToPojo(this.repository.getByFilter(filter));
        } catch (Exception e) {
            throw onException("Не удалось получить сущность", e);
        }
    }

    public List<T> getList() throws ServiceException {
        return getList(new ListFilter());
    }

    public List<T> getList(ListFilter listFilter) throws ServiceException {
        try {
            return this.repository.getListByFilter(listFilter).stream()
                    .map(transformer::convertEntityToPojo).collect(Collectors.toList());
        } catch (Exception e) {
            throw onException("Не удалось получить список сущностей", e);
        }
    }

    public void create(T pojo) throws ServiceException {
        try {
            TEntity entity = transformer.convertPojoToEntity(pojo);
            this.repository.save(entity);
        } catch (Exception e) {
            throw onException("Не удалось создать сущность", e);
        }
    }

    public void update(T pojo) throws ServiceException {
        try {
            TEntity entity = transformer.convertPojoToEntity(pojo);
            this.repository.save(entity);
        } catch (Exception e) {
            throw onException("Не удалось обновить сущность", e);
        }
    }

    public void delete(int id) throws ServiceException {
        try {
            this.repository.delete(id);
        } catch (Exception e) {
            throw onException("Не удалось удалить сущность", e);
        }
    }

    protected ServiceException onException(String message, Exception causeException) throws ServiceException {
        logger.error(causeException);
        return createServiceException(message);
    }

    protected abstract ServiceException createServiceException(String message);
}
