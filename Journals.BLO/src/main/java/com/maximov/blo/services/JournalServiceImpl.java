package com.maximov.blo.services;

import com.maximov.common.exceptions.*;
import com.maximov.dao.transformers.Transformer;
import com.maximov.common.models.pojo.Journal;
import com.maximov.dao.entities.JournalEntity;
import com.maximov.dao.repositories.JournalRepository;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class JournalServiceImpl extends CrudServiceImpl<Journal, JournalEntity> implements JournalService {

    @Autowired
    public JournalServiceImpl(JournalRepository repository, Transformer<Journal, JournalEntity> transformer) {
        super(repository, transformer);
    }

    @Override
    protected ServiceException createServiceException(String message) {
        return new JournalServiceException(message);
    }
}
