package com.maximov.blo.config;

import com.maximov.blo.services.*;
import com.maximov.dao.config.RootDaoConfig;
import org.springframework.context.annotation.*;
import org.springframework.remoting.rmi.RmiServiceExporter;


@Configuration
@ComponentScan(basePackages = {"com.maximov.blo"})
@Import({SecurityConfig.class, RootDaoConfig.class})
public class RootConfig {

    private static final int PORT = 1200;

    @Bean
    public RmiServiceExporter authorizationServiceExporter(AuthorizationService service){
        return createRmiServiceExporter(service, AuthorizationService.class);
    }

    @Bean
    public RmiServiceExporter emailServiceExporter(EmailService service){
        return createRmiServiceExporter(service, EmailService.class);
    }

    @Bean
    public RmiServiceExporter journalServiceExporter(JournalService service){
        return createRmiServiceExporter(service, JournalService.class);
    }

    @Bean
    public RmiServiceExporter noteServiceExporter(NoteService service){
        return createRmiServiceExporter(service, NoteService.class);
    }

    @Bean
    public RmiServiceExporter personServiceExporter(PersonService service){
        return createRmiServiceExporter(service, PersonService.class);
    }

    @Bean
    public RmiServiceExporter userDetailsServiceExporter(LocalUserDetailsService service){
        return createRmiServiceExporter(service, LocalUserDetailsService.class);
    }

    private <T extends Service>RmiServiceExporter createRmiServiceExporter(T service, Class<T> interfaceType) {
        RmiServiceExporter rmiServiceExporter = new RmiServiceExporter();
        rmiServiceExporter.setService(service);
        rmiServiceExporter.setServiceName(interfaceType.getSimpleName());
        rmiServiceExporter.setServiceInterface(interfaceType);
        rmiServiceExporter.setRegistryPort(PORT);
        return rmiServiceExporter;
    }

}
