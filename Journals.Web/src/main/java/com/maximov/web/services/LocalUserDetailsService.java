package com.maximov.web.services;

import org.springframework.security.core.userdetails.UserDetailsService;

@org.springframework.stereotype.Service
public interface LocalUserDetailsService extends UserDetailsService,Service {
}
