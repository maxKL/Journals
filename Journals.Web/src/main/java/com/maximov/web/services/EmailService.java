package com.maximov.web.services;

/**
 * Сервис для посылки сообщений по электронной почте
 */
@org.springframework.stereotype.Service
public interface EmailService extends Service {
    void sendMessage(String address, String subject, String messageText);
}
