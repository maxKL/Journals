package com.maximov.web.services;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.filters.ListFilter;
import com.maximov.common.filters.SingleFilter;
import com.maximov.common.models.pojo.Note;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

/**
 * Сервис для работы с заметками
 */
@org.springframework.stereotype.Service
@Secured("ROLE_USER")
public interface NoteService extends CrudService<Note> {
    @Override
    default List<Note> getList() throws ServiceException {
        return null;
    }

    @Override
    default List<Note> getList(ListFilter listFilter) throws ServiceException {
        return null;
    }

    @Override
    default Note getById(int id) throws ServiceException {
        return null;
    }

    @Override
    default Note get(SingleFilter filter) throws ServiceException {
        return null;
    }

    @Override
    default void create(Note entity) throws ServiceException {

    }

    @Override
    default void update(Note entity) throws ServiceException {

    }

    @Override
    default void delete(int id) throws ServiceException {

    }
}
