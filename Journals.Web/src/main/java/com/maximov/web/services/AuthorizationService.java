package com.maximov.web.services;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.models.RegistrationInfo;

@org.springframework.stereotype.Service
public interface AuthorizationService extends Service {
    void register(RegistrationInfo registrationInfo) throws ServiceException;
}
