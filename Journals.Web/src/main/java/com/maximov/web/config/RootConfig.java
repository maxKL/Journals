package com.maximov.web.config;

import com.maximov.web.services.*;
import org.springframework.context.annotation.*;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.security.remoting.rmi.ContextPropagatingRemoteInvocationFactory;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages = {"com.maximov.web"}, excludeFilters = {
        @ComponentScan.Filter (type = FilterType.ANNOTATION, value = EnableWebMvc.class)})
public class RootConfig {

    private static final int PORT = 1200;

    @Bean
    public RmiProxyFactoryBean authorizationService(){
        return createRmiProxyFactoryBean(AuthorizationService.class);
    }

    @Bean
    public RmiProxyFactoryBean emailService(){
        return createRmiProxyFactoryBean(EmailService.class);
    }

    @Bean
    public RmiProxyFactoryBean journalService(){
        return createRmiProxyFactoryBean(JournalService.class);
    }

    @Bean
    public RmiProxyFactoryBean noteService(){
        return createRmiProxyFactoryBean(NoteService.class);
    }

    @Bean
    public RmiProxyFactoryBean personService(){
        return createRmiProxyFactoryBean(PersonService.class);
    }

    @Bean
    public RmiProxyFactoryBean userDetailsService(){
        return createRmiProxyFactoryBean(LocalUserDetailsService.class);
    }

    private <T extends Service> RmiProxyFactoryBean createRmiProxyFactoryBean(Class<T> serviceClass) {
        RmiProxyFactoryBean rmiProxy = new RmiProxyFactoryBean();
        rmiProxy.setServiceUrl(String.format("rmi://localhost:%1$d/%2$s", PORT, serviceClass.getSimpleName()));
        rmiProxy.setServiceInterface(serviceClass);
        rmiProxy.setRemoteInvocationFactory(remoteInvocationFactory());
        return rmiProxy;
    }

    @Bean
    public ContextPropagatingRemoteInvocationFactory remoteInvocationFactory(){
        return new ContextPropagatingRemoteInvocationFactory();
    }
}
