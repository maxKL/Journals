package com.maximov.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService,
                          AuthenticationSuccessHandler authenticationSuccessHandler) {
        this.userDetailsService = userDetailsService;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .formLogin()
                .loginPage("/login")
                .successHandler(this.authenticationSuccessHandler)
            .and()
            .logout()
                .logoutSuccessUrl("/login")
            .and()
            .authorizeRequests()
                .antMatchers("/", "/journal").hasAnyRole("USER")
                .antMatchers(HttpMethod.POST,"/journal", "/journal/delete").hasRole("USER")
                .antMatchers("/notePage", "/note").hasRole("USER")
                .antMatchers(HttpMethod.POST,"/note**").hasRole("USER")
                .antMatchers("/persons", "/person").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/person**").hasRole("ADMIN")
                .anyRequest().permitAll()
            .and()
            .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new Md5PasswordEncoder());
        auth.eraseCredentials(false);
    }
}
