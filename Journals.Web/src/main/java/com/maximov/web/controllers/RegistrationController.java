package com.maximov.web.controllers;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.models.RegistrationInfo;
import com.maximov.web.services.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {
    private AuthorizationService service;

    @Autowired
    public void setService(AuthorizationService service) {
        this.service = service;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showLoginPage() {
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView doPost(RegistrationInfo info) throws ServiceException {
        service.register(info);
        return new ModelAndView("redirect:/login");
    }
}
