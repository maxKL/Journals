package com.maximov.web.controllers;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.filters.NoteListFilter;
import com.maximov.common.models.pojo.Journal;
import com.maximov.common.models.pojo.Note;
import com.maximov.web.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Контроллер для работы с записями в дневнике
 */
@Controller
public class NoteController {
    private NoteService service;

    @Autowired
    public void setService(NoteService service) {
        this.service = service;
    }

    @RequestMapping(value = "/notesPage", method = RequestMethod.GET)
    public String notes(HttpSession httpSession, Model model) throws ServiceException {
        Journal journal = (Journal) httpSession.getAttribute("journal");
        List<Note> noteList = service.getList(new NoteListFilter(journal.getId()));
        model.addAttribute("notes", noteList);

        return "notes";
    }

    @RequestMapping(value = "/note", method = RequestMethod.GET)
    public String showJournalForm(Model model,
                                  @RequestParam(name = "id", required = false) Integer id) throws ServiceException {
        Note note = new Note();
        if(id != null && id > 0) {
            model.addAttribute("mode", "update");
            note = service.getById(id);
        }else{
            model.addAttribute("mode", "create");
        }
        model.addAttribute("note", note);

        return "noteForm";
    }

    @RequestMapping(value = "/note", method = RequestMethod.POST)
    public ModelAndView postNote(HttpSession httpSession, Note note) throws ServiceException {
        Journal journal = (Journal) httpSession.getAttribute("journal");
        note.setJournal(journal);
        note.setDate(LocalDateTime.now());
        if(note.getId() == 0) {
            service.create(note);
        } else{
            service.update(note);
        }

        return new ModelAndView("redirect:/notesPage");
    }

    @RequestMapping(value = "/note/delete", method = RequestMethod.POST)
    public void deleteNote(@RequestParam(name = "id") int id) throws ServiceException {
        this.service.delete(id);
    }
}
