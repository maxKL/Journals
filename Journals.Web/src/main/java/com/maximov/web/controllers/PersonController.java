package com.maximov.web.controllers;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.models.enums.Role;
import com.maximov.common.models.pojo.Person;
import com.maximov.web.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PersonController {
    private PersonService service;

    @Autowired
    public void setService(PersonService service) {
        this.service = service;
    }

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public String persons(Model model) throws ServiceException {
        List<Person> personList = service.getList();
        model.addAttribute("persons", personList);

        return "persons";
    }

    @RequestMapping(value = "/person/block", method = RequestMethod.POST)
    public void blockPerson(@RequestParam(name = "id") int id) throws ServiceException {
        this.service.blockPerson(id);
    }

    @RequestMapping(value = "/person/unblock", method = RequestMethod.POST)
    public void unblockPerson(@RequestParam(name = "id") int id) throws ServiceException {
        this.service.unblockPerson(id);
    }

    @RequestMapping(value = "/person/setAdmin", method = RequestMethod.POST)
    public void setAdmin(@RequestParam(name = "id") int id) throws ServiceException {
        this.service.setRole(id, Role.ROLE_ADMIN);
    }

    @RequestMapping(value = "/person/setUser", method = RequestMethod.POST)
    public void setUser(@RequestParam(name = "id") int id) throws ServiceException {
        this.service.setRole(id, Role.ROLE_USER);
    }
}
