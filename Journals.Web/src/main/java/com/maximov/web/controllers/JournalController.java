package com.maximov.web.controllers;

import com.maximov.common.exceptions.ServiceException;
import com.maximov.common.models.UserPrincipal;
import com.maximov.common.filters.JournalListFilter;
import com.maximov.common.models.pojo.Journal;
import com.maximov.common.models.pojo.Person;
import com.maximov.web.services.JournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class JournalController {
    private JournalService service;

    @Autowired
    public void setService(JournalService service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String journals(Authentication authentication, Model model) throws ServiceException {
        UserPrincipal user = (UserPrincipal) authentication.getPrincipal();
        Integer personId = user.getId();
        List<Journal> journalList = service.getList(new JournalListFilter(personId));
        model.addAttribute("journals", journalList);

        return "journals";
    }

    @RequestMapping(value = "/journal", method = RequestMethod.GET)
    public String showJournalForm(Model model,
                              @RequestParam(name = "id", required = false) Integer id) throws ServiceException {
        Journal journal = new Journal();
        if(id != null && id > 0) {
            model.addAttribute("mode", "update");
            journal = service.getById(id);
        }else{
            model.addAttribute("mode", "create");
        }

        if(journal.getTitle() == null)
            journal.setTitle("");

        model.addAttribute("journal", journal);

        return "journalForm";
    }

    @RequestMapping(value = "/journal", method = RequestMethod.POST)
    public ModelAndView postJournal(HttpServletRequest req, Journal journal, Authentication authentication) throws ServiceException {
        Person owner = new Person();
        if(journal.getId() == 0) {
            UserPrincipal user = (UserPrincipal) authentication.getPrincipal();
            owner.setId(user.getId());
            journal.setPerson(owner);
            service.create(journal);
        } else{
            owner.setId(Integer.parseInt(req.getParameter("personId")));
            journal.setPerson(owner);
            service.update(journal);
        }

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/journal/delete", method = RequestMethod.POST)
    public void deleteJournal(@RequestParam(name = "id") int id) throws ServiceException {
        this.service.delete(id);
    }

    @RequestMapping(value = "/goToNotes", method = RequestMethod.GET)
    public ModelAndView goToNotes(HttpSession httpSession, int journalId) throws ServiceException {
        httpSession.setAttribute("journal", service.getById(journalId));
        return new ModelAndView("redirect:/notesPage");
    }
}
