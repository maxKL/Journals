package com.maximov.web.listeners;

import com.maximov.common.models.enums.Role;
import com.maximov.common.models.pojo.Person;
import com.maximov.web.services.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.*;


public class SessionListener implements HttpSessionAttributeListener, ServletContextListener {
    private Logger logger = Logger.getLogger(SessionListener.class);
    private EmailService emailService;
    private PersonService personService;

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        try {
            if (event.getName().equals("userid"))
            {
                int userid = (int) event.getValue();
                Person user = this.personService.getById(userid);
                if(user.getRole() == Role.ROLE_ADMIN) {
                    this.emailService.sendMessage(user.getEmail(), "LogIN",
                            String.format("Dear %s. You have logged in on Journals IS", user.getLogin()));
                }
            }
        } catch (Exception ex){
            logger.error(ex);
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {

    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebApplicationContextUtils
                .getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
