package com.maximov.web.handlers;

import com.maximov.common.exceptions.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    private Logger logger = Logger.getLogger(GlobalDefaultExceptionHandler.class);
    public static final String DEFAULT_ERROR_VIEW = "error";

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation
                (e.getClass(), ResponseStatus.class) != null)
            throw e;

        logger.error(e);

        ModelAndView mav = new ModelAndView();
        mav.addObject("errorMessage", getMessageToShow(e));
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }

    private String getMessageToShow(Throwable throwable){
        return isShowableException(throwable) ? throwable.getMessage() : null;
    }

    private boolean isShowableException(Throwable throwable){
        return throwable != null && ServiceException.class.isAssignableFrom(throwable.getClass());
    }
}
