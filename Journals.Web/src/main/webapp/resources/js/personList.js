function blockPerson(id){
    $.post("/journals/person/block", $.param({id: id}), function(){
        location.reload();
    })
}
function unblockPerson(id){
    $.post("/journals/person/unblock", $.param({id: id}), function(){
        location.reload();
    })
}
function setAdmin(id){
    $.post("/journals/person/setAdmin", $.param({id: id}), function(){
        location.reload();
    })
}
function setUser(id){
    $.post("/journals/person/setUser", $.param({id: id}), function(){
        location.reload();
    })
}