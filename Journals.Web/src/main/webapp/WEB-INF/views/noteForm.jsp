<%@ page import="com.maximov.common.models.pojo.Note" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    Note note = (Note)request.getAttribute("note");
    String mode = (String) request.getAttribute("mode");
%>
<head>
    <title><%= mode.equals("create") ? "Создание новой заметки" : "Редактирование заметки" %></title>
</head>
<body>
<form action="/journals/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<form action="/journals/note" method="post">
    <textarea required id="text" name="text">${note.getText()}</textarea>
    <input hidden id="id" name="id" value="<%= note.getId() %>" />
    <br/>
    <input type="submit" value="Сохранить" />
</form>
</body>
</html>
