<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.maximov.common.models.pojo.Journal" %>
<html>
<%
    Journal journal = (Journal)request.getAttribute("journal");
    String mode = (String) request.getAttribute("mode");
%>
<head>
    <title><%= mode.equals("create") ? "Создание нового журнала" : "Редактирование данных о журнале" %></title>
</head>
<body>
<form action="/journals/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<form action="/journals/journal" method="post">
    <label for="title">Название: </label>
    <input required id="title" name="title" value="${journal.getTitle()}"> <br/>

    <input hidden id="id" name="id" value="${journal.getId()}"> <br/>
    <input hidden id="version" name="version" value="${journal.getVersion()}"> <br/>
    <input hidden id="personId" name="personId" value="${mode.equals("create") ? 0 : journal.getPerson().getId()}"> <br/>


    <input type="submit" value="Сохранить" />
</form>
</body>
</html>
