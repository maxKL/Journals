<%@ page import="com.maximov.common.models.pojo.Journal" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    Journal journal = (Journal)session.getAttribute("journal");
%>
<head>
    <title>Записи журнала "<%= journal.getTitle() %>"</title>
    <script src="<c:url value="/resources/js/vendor/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/resources/js/notesList.js" />"></script>
</head>
<body>
<form action="/journals/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<a href="/journals/">К списку дневников</a><br/>
<table>
    <c:forEach items="${notes}" var="note">
        <tr id="<c:out  value="${note.id}"></c:out>">
            <td><c:out value="${note.date}"></c:out></td>
            <td><c:out value="${note.text}"></c:out></td>
            <td><button onclick="deleteNote(<c:out  value="${note.id}"></c:out>)">Удалить</button></td>
            <td><a href="note?id=<c:out  value="${note.id}"></c:out>">Редактировать</a></td>
        </tr>
    </c:forEach>
</table>
<a href="note">Добавить</a>
</body>
</html>
