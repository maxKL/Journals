<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<form action="/journals/registration" method="post">
    <table>
        <tr>
            <td><label for="password">E-mail:</label></td>
            <td><input required type="email" name="email" id="email" value="" /></td>
        </tr>
        <tr>
            <td><label for="login">Login:</label></td>
            <td><input required type="text" name="login" id="login" value="" /></td>
        </tr>
        <tr>
            <td><label for="password">Пароль:</label></td>
            <td><input required type="password" name="password" id="password" value="" /></td>
        </tr>
        <tr>
            <td><input type="submit" value="OK" /></td>
        </tr>
    </table>
</form>
</body>
</html>
