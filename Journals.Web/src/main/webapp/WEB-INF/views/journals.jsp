<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Journals List</title>
    <script src="<c:url value="/resources/js/vendor/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/resources/js/journalsList.js" />"></script>
</head>
<body>
<form action="/journals/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<table>
    <tr>
        <td>Название</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <c:forEach items="${journals}" var="journal">
        <tr id="<c:out  value="${journal.id}"></c:out>">
            <td><c:out value="${journal.title}"></c:out></td>
            <td><button onclick="deleteJournal(<c:out  value="${journal.id}"></c:out>)">Удалить</button></td>
            <td><a href="journal?id=<c:out  value="${journal.id}"></c:out>">Редактировать</a></td>
            <td>
                <a href="/journals/goToNotes?journalId=<c:out  value="${journal.id}"></c:out>">Перейти к записям</a>
            </td>
        </tr>
    </c:forEach>
</table>
<a href="journal">Создать</a>
</body>
</html>
