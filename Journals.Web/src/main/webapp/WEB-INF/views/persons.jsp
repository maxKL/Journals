<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.maximov.common.models.enums.Role"%>
<html>
<head>
    <title>Persons</title>
    <script src="<c:url value="/resources/js/vendor/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/resources/js/personList.js" />"></script>
</head>
<body>
<form action="/journals/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<table>
    <tr>
        <td>Login</td>
        <td>Email</td>
        <td>Role</td>
        <td></td>
        <td></td>
    </tr>
    <c:forEach items="${persons}" var="person">
        <tr id="${person.id}">
            <td>${person.login}</td>
            <td>${person.email}</td>
            <td>${person.role}</td>
            <c:if test="${!person.block}">
                <td><button onclick="blockPerson(${person.id})">Блокировать</button></td>
            </c:if>
            <c:if test="${person.block}">
                <td><button onclick="unblockPerson(${person.id})">Разблокировать</button></td>
            </c:if>
            <c:if test="${person.role == 'ROLE_USER'}">
                <td><button onclick="setAdmin(${person.id})">Сделать администратором</button></td>
            </c:if>
            <c:if test="${person.role == 'ROLE_ADMIN'}">
                <td><button onclick="setUser(${person.id})">Сделать пользователем</button></td>
            </c:if>
        </tr>
    </c:forEach>
</table>
</body>
</html>
