<%@ page contentType="text/html;charset=UTF-8" language="java" session="false"%>
<html>
<head>
    <title>Error</title>
</head>
<body>
<form action="/journals/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<% String message = (String)request.getAttribute("errorMessage");%>
<%= message == null || message.trim().equals("") ? "Произошда техническая ошибка" : message %>
</body>
</html>
