<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<div>
    <form name="f" action="/journals/login" method="post">
        <table>
            <tr>
                <td><label for="username">Login:</label></td>
                <td><input required type="text" name="username" id="username" value="" /></td>
            </tr>
            <tr>
                <td><label for="password">Пароль:</label></td>
                <td><input required type="password" name="password" id="password" value="" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="login" /></td>
            </tr>
        </table>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
    <a href="registration">Регистрация</a>
</div>
</body>
</html>
