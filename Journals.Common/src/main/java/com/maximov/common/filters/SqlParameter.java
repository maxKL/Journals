package com.maximov.common.filters;

import java.sql.SQLType;

public class SqlParameter {
    private Object value;
    private SQLType type;

    public SqlParameter(Object value, SQLType type) {
        this.value = value;
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public SQLType getType() {
        return type;
    }
}
