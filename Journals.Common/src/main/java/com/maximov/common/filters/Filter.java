package com.maximov.common.filters;

import java.io.Serializable;

public abstract class Filter implements Serializable {
    protected String[] fields = new String[0];

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    public abstract SelectSettings getSelectSettings();
}
