package com.maximov.common.filters;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class SelectSettings {
    private String selectParamsString = "";
    private Map<String, SqlParameter> selectParams = new HashMap<>();

    public SelectSettings(){

    }

    public boolean isEmpty(){
        return StringUtils.isEmpty(selectParamsString) || selectParams == null || selectParams.size() == 0;
    }

    public String getSelectParamsString() {
        return selectParamsString;
    }

    public Map<String, SqlParameter> getSelectParams() {
        return selectParams;
    }

    public void addParameter(String name, SqlParameter value){
        selectParamsString += (selectParamsString.equals("") ? "" : " AND ") + (name + " = :" + name);
        selectParams.put(name, value);
    }

}
