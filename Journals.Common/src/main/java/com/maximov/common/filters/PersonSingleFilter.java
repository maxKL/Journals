package com.maximov.common.filters;


import org.apache.commons.lang3.StringUtils;

import java.sql.JDBCType;

public class PersonSingleFilter extends SingleFilter {
    private String login;
    private String password;

    public PersonSingleFilter(String login){
        this.login = login;
    }

    public PersonSingleFilter(String login, String password) {
        this(login);
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isCorrect() {
        return super.isCorrect() || !StringUtils.isEmpty(this.login);
    }

    @Override
    public SelectSettings getSelectSettings() {
        SelectSettings settings = super.getSelectSettings();

        if(StringUtils.isNotEmpty(login))
            settings.addParameter("login", new SqlParameter(login, JDBCType.VARCHAR));
        if(StringUtils.isNotEmpty(password))
            settings.addParameter("password", new SqlParameter(password, JDBCType.VARCHAR));

        return settings;
    }
}
