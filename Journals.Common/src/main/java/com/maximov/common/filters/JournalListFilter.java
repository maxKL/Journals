package com.maximov.common.filters;

import java.sql.JDBCType;

/**
 * Фильтр дневников
 */
public class JournalListFilter extends ListFilter {
    private Integer personId;

    public JournalListFilter(Integer personId) {
        this.personId = personId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    @Override
    public SelectSettings getSelectSettings(){
        SelectSettings selectSettings = super.getSelectSettings();

        if(personId > 0){
            selectSettings.addParameter("personid", new SqlParameter(personId, JDBCType.INTEGER));
        }

        return selectSettings;
    }
}
