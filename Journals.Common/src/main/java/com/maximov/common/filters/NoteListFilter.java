package com.maximov.common.filters;

import java.sql.JDBCType;

/**
 * Фильтр для выборки списка сущностей
 */
public class NoteListFilter extends ListFilter {
    private int journalId;

    public NoteListFilter(int journalId) {
        this.journalId = journalId;
    }

    public int getJournalId() {
        return journalId;
    }

    public void setJournalId(int journalId) {
        this.journalId = journalId;
    }

    @Override
    public SelectSettings getSelectSettings(){
        SelectSettings selectSettings = super.getSelectSettings();

        if(journalId > 0){
            selectSettings.addParameter("journalid", new SqlParameter(journalId, JDBCType.INTEGER));
        }

        return selectSettings;
    }
}
