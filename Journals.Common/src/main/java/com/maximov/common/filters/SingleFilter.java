package com.maximov.common.filters;

import java.sql.JDBCType;

public class SingleFilter extends Filter {
    private int id;

    public SingleFilter(int id) {
        this.id = id;
    }

    public SingleFilter() {
    }

    public boolean isCorrect() {
        return id > 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public SelectSettings getSelectSettings() {

        SelectSettings selectSettings = new SelectSettings();

        if(id > 0){
            selectSettings.addParameter("id", new SqlParameter(id, JDBCType.INTEGER));
        }

        return selectSettings;
    }
}
