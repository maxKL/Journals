package com.maximov.common.exceptions;

/**
 * Ошыбка сервиса авторизации
 */
public class AuthorizeServiceException extends ServiceException {
    public AuthorizeServiceException(String message) {
        super(message);
    }
}
