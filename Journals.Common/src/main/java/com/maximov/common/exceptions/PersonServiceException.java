package com.maximov.common.exceptions;

/**
 * Ошибка сервиса по работе с пользователями
 */
public class PersonServiceException extends ServiceException {
    public PersonServiceException(String message) {
        super(message);
    }
}
