package com.maximov.common.exceptions;

/**
 * Ошибка при работе с дневниками
 */
public class JournalServiceException extends ServiceException {
    public JournalServiceException(String message) {
        super(message);
    }
}
