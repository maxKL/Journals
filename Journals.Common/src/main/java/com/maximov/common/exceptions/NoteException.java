package com.maximov.common.exceptions;

/**
 * Исключение при работе с заметками
 */
public class NoteException extends ServiceException {
    public NoteException(String message) {
        super(message);
    }
}
