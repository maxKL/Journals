package com.maximov.common.models.enums;

/**
 * Роль пользователя
 */
public enum Role {
    INVALID(-1),
    ROLE_USER(0),
    ROLE_ADMIN(1);

    private final short value;

    Role(int value) {
        this.value = (short)value;
    }

    public short getValue() {
        return value;
    }

    public static synchronized Role fromShort(short value){
        switch (value) {
            case 0:
                return Role.ROLE_USER;
            case 1:
                return Role.ROLE_ADMIN;
            default:
                return Role.INVALID;
        }
    }
}
