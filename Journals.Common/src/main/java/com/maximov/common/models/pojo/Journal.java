package com.maximov.common.models.pojo;

public class Journal extends SimplePojo {
    private Person person;
    private String title;

    public Journal() {
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
