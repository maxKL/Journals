package com.maximov.common.models.pojo;

import java.time.LocalDateTime;

public class Note extends SimplePojo {
    private Journal journal;
    private LocalDateTime date;
    private String text;

    public Note() {
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
