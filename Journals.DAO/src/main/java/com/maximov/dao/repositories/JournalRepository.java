package com.maximov.dao.repositories;

import com.maximov.dao.entities.JournalEntity;

public interface JournalRepository extends CommonRepository<JournalEntity> {
}
