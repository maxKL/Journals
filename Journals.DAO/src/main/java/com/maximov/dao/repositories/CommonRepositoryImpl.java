package com.maximov.dao.repositories;

import com.maximov.common.filters.*;
import org.apache.log4j.Logger;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;

/**
 * Репозиторий для работы с определённой таблицей в базе данных
 */
public class CommonRepositoryImpl<T extends com.maximov.dao.entities.Entity>
        extends SimpleJpaRepository<T, Integer> implements CommonRepository<T> {
    protected Logger logger = Logger.getLogger(this.getClass());

    private EntityManager entityManager;
    private Class<T> domainClass;

    public CommonRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.domainClass = entityInformation.getJavaType();
    }

    public T getByFilter(SingleFilter filter) {
        if (!filter.isCorrect())
            throw new IllegalArgumentException("Invalid filter");

        SelectSettings selectSettings = filter.getSelectSettings();
        if (selectSettings.isEmpty())
            throw new IllegalArgumentException("Invalid filter");

        try {
            String hqlQuery = String.format("FROM %s WHERE %s",
                    domainClass.getSimpleName(), selectSettings.getSelectParamsString());

            TypedQuery<T> typedQuery = setSelectSettings(hqlQuery, selectSettings);

            return typedQuery.getSingleResult();
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    public List<T> getListByFilter(ListFilter filter) {
        try {
            String hqlQuery = String.format("FROM %s",
                    domainClass.getSimpleName());

            SelectSettings selectSettings = filter.getSelectSettings();
            if (!selectSettings.isEmpty()) {
                hqlQuery += String.format(" WHERE %s", selectSettings.getSelectParamsString());
            }

            TypedQuery<T> typedQuery = setSelectSettings(hqlQuery, selectSettings);
            return typedQuery.getResultList();
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    private TypedQuery<T> setSelectSettings(String hqlQuery, SelectSettings selectSettings) {
        TypedQuery<T> typedQuery = entityManager.createQuery(hqlQuery, domainClass);
        for (Map.Entry<String, SqlParameter> entry : selectSettings.getSelectParams().entrySet()) {
            typedQuery.setParameter(entry.getKey(), entry.getValue().getValue());
        }
        return typedQuery;
    }
}

