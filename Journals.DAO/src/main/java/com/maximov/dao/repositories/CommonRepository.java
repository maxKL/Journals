package com.maximov.dao.repositories;

import com.maximov.common.filters.ListFilter;
import com.maximov.common.filters.SingleFilter;
import com.maximov.dao.entities.Entity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Репозиторий для работы с определённой таблицей в базе данных
 */
public interface CommonRepository<T extends Entity> extends CrudRepository<T, Integer> {
    T getByFilter(SingleFilter filter);

    List<T> getListByFilter(ListFilter filter);
}
