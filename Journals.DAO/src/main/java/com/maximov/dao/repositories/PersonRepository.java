package com.maximov.dao.repositories;

import com.maximov.dao.entities.PersonEntity;

/**
 * Репозиторий для работы со списком пользователей
 */
public interface PersonRepository extends CommonRepository<PersonEntity> {
}
