package com.maximov.dao.repositories;

import com.maximov.dao.entities.NoteEntity;

public interface NoteRepository extends CommonRepository<NoteEntity> {
}
