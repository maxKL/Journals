package com.maximov.dao.entities;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Table;
import java.util.Set;

/**
 * Сущность пользователя
 */
@javax.persistence.Entity
@Table(name = "person", schema = "public")
@SequenceGenerator(name = "default_gen", schema = "public", sequenceName = "person_id_seq", allocationSize = 1)
public class PersonEntity extends Entity {
    @Column(name = "login")
    private String login;

    @Column(name="password")
    private String password;

    @Column(name="role")
    private Short role;

    @Column(name="email")
    private String email;

    @Column(name="block")
    private boolean block;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
    @Cascade(CascadeType.DELETE)
    private Set<JournalEntity> journals;

    public PersonEntity() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Short getRole() {
        return role;
    }

    public void setRole(Short role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public Set<JournalEntity> getJournals() {
        return journals;
    }

    public void setJournals(Set<JournalEntity> journals) {
        this.journals = journals;
    }
}
