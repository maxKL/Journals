package com.maximov.dao.entities;


import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Сущность записи
 */
@javax.persistence.Entity
@Table(name = "note", schema = "public")
@SequenceGenerator(name = "default_gen", schema = "public", sequenceName = "note_id_seq", allocationSize = 1)
public class NoteEntity extends Entity {
    @ManyToOne
    @JoinColumn(name = "journalid")
    private JournalEntity journal;

    @Column(name= "date")
    private LocalDateTime date;

    @Column(name = "text")
    private String text;

    public NoteEntity() {
    }

    public JournalEntity getJournal() {
        return journal;
    }

    public void setJournal(JournalEntity journal) {
        this.journal = journal;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
