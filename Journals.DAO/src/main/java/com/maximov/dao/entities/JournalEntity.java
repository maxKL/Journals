package com.maximov.dao.entities;


import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Table;;
import java.util.Set;

/**
 * Сущность дневника
 */
@javax.persistence.Entity
@Table(name = "journal", schema = "public")
@SequenceGenerator(name = "default_gen", schema = "public", sequenceName = "journal_id_seq", allocationSize = 1)
public class JournalEntity extends Entity {
    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "personid")
    private PersonEntity person;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "journal")
    @Cascade(CascadeType.DELETE)
    private Set<NoteEntity> notes;

    public JournalEntity() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public Set<NoteEntity> getNotes() {
        return notes;
    }

    public void setNotes(Set<NoteEntity> notes) {
        this.notes = notes;
    }
}
