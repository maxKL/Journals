package com.maximov.dao.transformers;

import com.maximov.common.models.pojo.Journal;
import com.maximov.common.models.pojo.Person;
import com.maximov.dao.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JournalTransformer implements Transformer<Journal, JournalEntity> {
    @PersistenceContext
    private EntityManager entityManager;

    private PersonTransformer personTransformer;

    @Autowired
    public void setPersonTransformer(PersonTransformer personTransformer) {
        this.personTransformer = personTransformer;
    }

    @Override
    public Journal convertEntityToPojo(JournalEntity entity) {
        Journal journal = new Journal();

        journal.setId(entity.getId());
        journal.setVersion(entity.getVersion());
        journal.setTitle(entity.getTitle());
        Person owner = personTransformer.convertEntityToPojo(entity.getPerson());
        journal.setPerson(owner);
        return journal;
    }

    @Override
    public JournalEntity convertPojoToEntity(Journal pojo) {
        JournalEntity journalEntity = new JournalEntity();

        journalEntity.setId(pojo.getId());
        journalEntity.setVersion(pojo.getVersion());
        journalEntity.setTitle(pojo.getTitle());
        PersonEntity owner = entityManager.getReference(PersonEntity.class, pojo.getPerson().getId());
        journalEntity.setPerson(owner);

        return journalEntity;
    }
}
