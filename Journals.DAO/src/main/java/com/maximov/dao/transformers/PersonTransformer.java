package com.maximov.dao.transformers;

import com.maximov.common.models.enums.Role;
import com.maximov.common.models.pojo.Person;
import com.maximov.dao.entities.PersonEntity;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PersonTransformer implements Transformer<Person, PersonEntity> {
    @Override
    public Person convertEntityToPojo(PersonEntity entity) {
        Person person = new Person();

        person.setId(entity.getId());
        person.setVersion(entity.getVersion());
        person.setBlock(entity.isBlock());
        person.setEmail(entity.getEmail());
        person.setRole(Role.fromShort(entity.getRole()));
        person.setLogin(entity.getLogin());
        person.setPassword(entity.getPassword());

        return person;
    }

    @Override
    public PersonEntity convertPojoToEntity(Person pojo) {
        PersonEntity personEntity = new PersonEntity();

        personEntity.setId(pojo.getId());
        personEntity.setVersion(pojo.getVersion());
        personEntity.setBlock(pojo.isBlock());
        personEntity.setEmail(pojo.getEmail());
        personEntity.setRole(pojo.getRole().getValue());
        personEntity.setLogin(pojo.getLogin());
        personEntity.setPassword(pojo.getPassword());

        return personEntity;
    }
}
