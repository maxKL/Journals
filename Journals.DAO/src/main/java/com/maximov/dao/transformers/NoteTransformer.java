package com.maximov.dao.transformers;

import com.maximov.common.models.pojo.Journal;
import com.maximov.common.models.pojo.Note;
import com.maximov.dao.entities.JournalEntity;
import com.maximov.dao.entities.NoteEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NoteTransformer implements Transformer<Note, NoteEntity> {
    @PersistenceContext
    private EntityManager entityManager;

    private JournalTransformer journalTransformer;

    @Autowired
    public void setJournalTransformer(JournalTransformer journalTransformer) {
        this.journalTransformer = journalTransformer;
    }

    @Override
    public Note convertEntityToPojo(NoteEntity entity) {
        Note note = new Note();

        note.setId(entity.getId());
        note.setVersion(entity.getVersion());
        note.setDate(entity.getDate());
        note.setText(entity.getText());
        Journal journal = this.journalTransformer.convertEntityToPojo(entity.getJournal());
        note.setJournal(journal);

        return note;
    }

    @Override
    public NoteEntity convertPojoToEntity(Note pojo) {
        NoteEntity noteEntity = new NoteEntity();

        noteEntity.setId(pojo.getId());
        noteEntity.setVersion(pojo.getVersion());
        noteEntity.setDate(pojo.getDate());
        noteEntity.setText(pojo.getText());
        JournalEntity journalEntity = entityManager.getReference(JournalEntity.class, pojo.getJournal().getId());
        noteEntity.setJournal(journalEntity);

        return noteEntity;
    }
}
