package com.maximov.dao.transformers;

import com.maximov.common.models.pojo.SimplePojo;
import com.maximov.dao.entities.Entity;

public interface Transformer<T extends SimplePojo, TEntity extends Entity> {
    T convertEntityToPojo(TEntity entity);

    TEntity convertPojoToEntity(T pojo);
}
